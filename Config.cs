
namespace cli_example {
    public class AppConfig {
        public string item1 { get; set;}
        public bool item2 {get; set;}
        public int item3 {get; set;}
        public float item4 {get; set;}
        public double item5 {get; set;}
        public NestConfig nestConfig {get; set;}
    }
}