﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using McMaster.Extensions.CommandLineUtils;
using log4net;
using log4net.Config;
using System.Reflection;
using System.IO;


namespace cli_example
{

    class Program
    {
        private static readonly log4net.ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        static void Main(string[] args)
        {
            Console.WriteLine("Configuring Logger!");
            var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
            XmlConfigurator.Configure(logRepository, new FileInfo("log4net.config"));
            logger.Info("Logging Configuration Successful. Loaded log4net.config");

            var app = new CommandLineApplication();
            //give people help with --help
            app.HelpOption("-? | -h | --help");

            var configFilePath = app.Option("-x|--xconfig", "Location of the config file (for reading OR writing).", CommandOptionType.SingleValue);
            var imageFolderPath = app.Option("-i|--imageFolder", "Location where input images are located", CommandOptionType.SingleValue );
            var cube = app.Command("cube", config => {
                config.OnExecute(() => {

                    //TODO: properly validate CLI arguments
                   
                    if (!string.IsNullOrWhiteSpace(configFilePath.Value())) {

                        IConfigurationRoot configuration = new ConfigurationBuilder()
                                                                .AddJsonFile(configFilePath.Value(), optional: false)
                                                                .Build();


                        //TODO: Add output demonstrating successful parsing of config file

                        return 0;
                    }
                    else {
                        app.ShowHelp();
                        return 1;
                    }
                    
                });
                 config.HelpOption("-? | -h | --help"); //show help on --help
            });
        
            var calibrate = app.Command("calibrate", config => {
                config.OnExecute(() => {
                    
                    //TODO: Finish Me
                    string src = null; //load this from CLI
                    string dest = null;
                    var outputConfig = callibrate(src, dest);
                    app.ShowHelp();
                    return 1;
                });
                config.HelpOption("-? | -h | --help"); //show help on --help
            });

            var corrector = app.Command("correct-callibration", config => {
                config.OnExecute(() => {
                    if (!string.IsNullOrWhiteSpace(configFilePath.Value())) {
                        //TODO: use actual appConfig
                        AppConfig appConfig = new AppConfig();
                        string src = null;
                        string dest = "fake/file/path";
                        var outputConfig = correctCallibration(appConfig, src);
                        writeConfigToYamlFile(outputConfig, dest);
                        return 0;
                    }
                    else {
                        app.ShowHelp();
                        return 1;
                    }
                });
                config.HelpOption("-? | -h | --help"); //show help on --help
            });


            var result = app.Execute(args);
            Environment.Exit(result);

        }

        public static void writeConfigToYamlFile(AppConfig conf, string destinationFolder) {
            //TODO: finish me
        }

        public static AppConfig correctCallibration(AppConfig config, string inputFolderPath) {
            return null;
        }

        public static AppConfig callibrate(string inputFolderPath, string outputFolderPath) {
            return null;
        }

    }
}
